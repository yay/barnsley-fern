document.addEventListener('DOMContentLoaded', function () {
    var canvas = document.getElementById('canvas');
    var canvasWidth = canvas.width;
    var canvasHeight = canvas.height;
    var ctx = canvas.getContext('2d');
    var imageData = ctx.getImageData(0, 0, canvas.width, canvas.height);

    var data = imageData.data;

    var worker = new Worker('js/worker.js');

    worker.addEventListener('message', function (e) {
        console.log('Got points.');

        var points = e.data, index, p, gp;

        for (var i = 0, n = points.length; i < n; i++) {
            p = points[i];
            gp = {
                x: Math.floor(p.x * 80),
                y: Math.floor(p.y * 80)
            };
            index = ((-gp.y + canvasHeight) * canvasWidth + (gp.x + canvasWidth * 0.5)) * 4;
            data[index] = 0;      // red
            data[++index] = 255;  // green
            data[++index] = 0;    // blue
            data[++index] = 255;  // alpha
        }
        ctx.putImageData(imageData, 0, 0);
    }, false);

});