self.addEventListener('message', function (e) {
    self.postMessage(e.data);
}, false);

var p = {
    x: 0,
    y: 0
};

var i = 1, points = [], n;

while (true) {
    points.push(p);
    if (i % 10000 === 0) {
        self.postMessage(points);
        points = [];
    }

    n = Math.floor(Math.random() * 100);
    if (n < 4) {
        p = f1(p);
    } else if (n < 77) {
        p = f2(p);
    } else if (n < 91) {
        p = f3(p);
    } else {
        p = f4(p);
    }
    if (i > 700000) {
        break;
    }
    i++;
}

function f1(point) {
    with (point) {
        return {
            x: 0,
            y: 0.16*y
        }
    }
}

function f2(point) {
    with (point) {
        return {
            x: 0.85*x + 0.04*y,
            y: -0.04*x + 0.85*y + 1.6
        }
    }
}

function f3(point) {
    with (point) {
        return {
            x: 0.2*x - 0.26*y,
            y: 0.23*x + 0.22*y + 1.6
        }
    }
}

function f4(point) {
    with (point) {
        return {
            x: -0.15*x + 0.28*y,
            y: 0.26*x + 0.24*y + 0.44
        }
    }
}