document.addEventListener('DOMContentLoaded', function () {
    var canvas = document.getElementById('canvas'),
        canvasWidth = canvas.width,
        canvasHeight = canvas.height,
        ctx = canvas.getContext('2d'),
        imageData = ctx.getImageData(0, 0, canvas.width, canvas.height);

    var buf = new ArrayBuffer(imageData.data.length),
        buf8 = new Uint8ClampedArray(buf),
        data = new Uint32Array(buf),
        dataView = new DataView(buf);

    var worker = new Worker('js/worker.js'),
        counter = 0;

    worker.addEventListener('message', function (e) {
        var points = new Float32Array(e.data), // Transferable Objects
            n = points.length / 2,
            index, x, y, cx, cy;

        console.log('Got', counter += n, 'points.');

        for (var i = 0; i < n; i++) {
            x = points[i*2];
            y = points[i*2 + 1];
            cx = (x * 80) | 0;
            cy = (y * 80) | 0;

            index = (-cy + canvasHeight) * canvasWidth + (cx + canvasWidth * 0.5);
            // using faster 32-bit pixel manipulation
            // NOTE: this will only work on little-endian hardware
            data[index] =
                (255 << 24) |  // alpha
                (0 << 16)   |  // blue
                (255 << 8)  |  // green
                0;             // red
        }
        imageData.data.set(buf8);
        ctx.putImageData(imageData, 0, 0);
    }, false);

});