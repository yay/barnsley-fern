var batchSize = 10000,
    points, i, j, n, x, y, nx, ny;

x = 0;
y = 0;

for (j = 0; j < 70; j++) {
    points = new Float32Array(batchSize * 2);
    for (i = 0; i < batchSize; i++) {
        n = Math.floor(Math.random() * 100);
        if (n < 4) {
            nx = 0;
            ny = 0.16*y;
        } else if (n < 77) {
            nx = 0.85*x + 0.04*y;
            ny = -0.04*x + 0.85*y + 1.6;
        } else if (n < 91) {
            nx = 0.2*x - 0.26*y;
            ny = 0.23*x + 0.22*y + 1.6;
        } else {
            nx = -0.15*x + 0.28*y;
            ny = 0.26*x + 0.24*y + 0.44;
        }
        points[i*2] = nx;
        points[i*2 + 1] = ny;
        x = nx;
        y = ny;
    }
    // pass the array of dots using Structured Cloning
    self.postMessage(points);
}