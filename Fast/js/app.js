document.addEventListener('DOMContentLoaded', function () {
    var canvas = document.getElementById('canvas');
    var canvasWidth = canvas.width;
    var canvasHeight = canvas.height;
    var ctx = canvas.getContext('2d');
    var imageData = ctx.getImageData(0, 0, canvas.width, canvas.height);

    var data = imageData.data;

    var worker = new Worker('js/worker.js');

    worker.addEventListener('message', function (e) {
        console.log('Got points.');

        var points = e.data, // Receive the data passed using Structured Cloning
            index, x, y, cx, cy;

        for (var i = 0, n = points.length / 2; i < n; i++) {
            x = points[i*2];
            y = points[i*2 + 1];
            cx = (x * 80) | 0;
            cy = (y * 80) | 0;

            index = ((-cy + canvasHeight) * canvasWidth + (cx + canvasWidth * 0.5)) * 4;
            data[index] = 0;      // red
            data[++index] = 255;  // green
            data[++index] = 0;    // blue
            data[++index] = 255;  // alpha
        }
        ctx.putImageData(imageData, 0, 0);
    }, false);

});